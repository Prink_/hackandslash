# Hack & Slash

Ce projet a été réalisé dans le cadre du module Jeux Vidéo en coopération avec deux autres élèves. 

Le but était de créer un Hack & Slash simpliste du style de Diablo pour apprendre à utiliser Unity.

Comment jouer :
* Cliquez sur le sol pour déplacer le héros.
* Cliquez sur les ennemis pour les attaquer.
* Appuyez sur ‘A’ pour lancer l’attaque spéciale.

Mes contributions au projet ont été :
* **Gestion de la caméra** : Binding de la caméra au joueur, gestion du mouvement autour du joueur, gestion du Zoom.
* **Mouvement du personnage** : Utilisation du Nav Mesh, gestion du déplacement par click (via Ray Casting).
* **Système de combat** : Simple clic sur un adversaire pour l'attaquer, 'A' puis click pour lancer une attaque spéciale, gestion de la hitbox de l'attaque spéciale via un mesh.
* **Implémentation du menu** : Pause du jeu, Affichage des éléments du menu.
* **Implémentation de la transparence** des murs lorsqu’un mur se trouve entre le joueur et la caméra.
* **Implémentation de la visualisation des dégâts**.
* **Implémentation du HUD**.
* **Design de l’écran titre**.
