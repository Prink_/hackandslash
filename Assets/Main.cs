﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AudioListener.volume = 20.0f/100.0f;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void StartGame(){
		SceneManager.LoadScene ("Arena");
	}

	public void QuitGame(){
		Debug.Log("Quitte le jeu (ne marche pas dans l'éditeur)");
		Application.Quit ();
	}

	public void VolumeControl(float value){
		AudioListener.volume = value/100.0f;
	}
}
