﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillHelperController : MonoBehaviour {
    private List<Collider> EnemyList;
    bool inUse;

	// Use this for initialization
	void Start () {
        inUse = true;
        EnemyList = new List<Collider>();
	}
	

	// Update is called once per frame
	void Update () {
        if (!inUse)
        {
            Destroy(this.gameObject);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && !EnemyList.Contains(other))
        {
            EnemyList.Add(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (EnemyList.Contains(other))
        {
            EnemyList.Remove(other);
        }
    }

    public List<Collider> skillUse()
    {
        inUse = false;
        return EnemyList;
    }

    public void skillCancel()
    {
        inUse = false;
    }
}
