﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

	protected GameObject mainCam;
	protected GameObject player;
	protected GameObject CleaveHelperPrefab;
    GameObject CleaveHelperInstance;
	GameObject PauseMenu;

    public enum State
	{
		normal, pause, useSkill, titleScreen
	}
	public State state;
	public State last_state;

	// Use this for initialization
	void Start () {
		mainCam = GameObject.FindGameObjectWithTag ("MainCamera");
		player = GameObject.FindGameObjectWithTag ("Player");
		state = State.normal;
        CleaveHelperPrefab = Resources.Load("Prefab/CleaveHelper") as GameObject;
		PauseMenu = GameObject.Find ("PauseMenu");
		if (PauseMenu != null) {
			PauseMenu.SetActive (false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		switch(state){
			case State.normal:
				M1_move ();
				M2_rotateCamera ();
				A_ActivateSkillHelper ();
				Escape_pause ();
				break;
			case State.useSkill:
				RaycastHit hit;
				Ray ray = mainCam.GetComponent<Camera> ().ScreenPointToRay (Input.mousePosition);
				if (Physics.Raycast (ray, out hit, 100.0f, 1 << 8) && CleaveHelperInstance != null) {
					CleaveHelperInstance.transform.LookAt (hit.point);
				}
				Escape_pause ();
                M1_lauchSkill_01();
                M2_cancelSkill();
            	break;
			case State.pause:
				Escape_resume ();
				break;
		}
	}

    /// Normal
	void M1_move(){
		if (Input.GetMouseButton (0)) {
			RaycastHit hit;
			Ray ray = mainCam.GetComponent<Camera> ().ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				player.GetComponent<playerController> ().setTarget (hit);
			}
		}
	}

	void M2_rotateCamera(){
		if (Input.GetMouseButton (1)) {
			mainCam.GetComponent<cameraController> ().rotation += Input.GetAxis ("Mouse X") / 2.0f;
		}
	}

    void A_ActivateSkillHelper()
    {
        if (Input.GetKeyDown("a"))
        {
            CleaveHelperInstance = Instantiate(CleaveHelperPrefab);
            CleaveHelperInstance.transform.parent = player.transform;
            CleaveHelperInstance.transform.localPosition = Vector3.zero;
			state = State.useSkill;
        }
    }

	void Escape_pause(){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			last_state = state;
			if(PauseMenu != null)
				PauseMenu.SetActive (true);
			state = State.pause;
			Time.timeScale = 0.0f;
		}
	}

    /// Using Skill a
    void M1_lauchSkill_01()
    {
        if (Input.GetMouseButtonDown(0))
        {
            List<Collider> mob = CleaveHelperInstance.GetComponent<SkillHelperController>().skillUse();
			if (mob.Count > 0) {
				player.GetComponent<ActorStats> ().cleaveAttack (mob);
				player.GetComponent<playerController> ().skill_01 (CleaveHelperInstance.transform.forward);
			}
        }
		if (Input.GetMouseButtonUp(0) && CleaveHelperInstance == null)
        {
            state = State.normal;
        }
    }

    void M2_cancelSkill()
    {
        if (Input.GetMouseButton(1))
        {
			if(CleaveHelperInstance != null)
            	CleaveHelperInstance.GetComponent<SkillHelperController>().skillCancel();
            state = State.normal;
        }
    }

	//Paused
	void Escape_resume(){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			state = last_state;
			if(PauseMenu != null)
				PauseMenu.SetActive (false);
			Time.timeScale = 1.0f;
		}
	}
}
