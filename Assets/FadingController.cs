﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Le but de cette classe est de rendre un objet transparent puis, après quelques instants,
/// de le rendre de nouveau totalement opaque
/// </summary>
public class FadingController : MonoBehaviour {
	public float min_alpha = 0.2f;
	public float max_alpha = 1.0f;
	public float fading_speed = 0.1f;
    
	private Renderer R;
	private Color c = Color.blue;
	private float fadingTimer = 0.0f;


	//Change le mode de rendu du matériau de l'objet en mode fade et le rend translucide.
    public void makeTranslucent()
    {
		if (R == null)
			return;
		ChangeRenderMode(R.material, BlendMode.Fade);
		c = R.material.color;
		if (c.a >= min_alpha) {
			c.a -= fading_speed;
			R.material.color = c;
		}
		fadingTimer = 0.1f;
    }


	// Use this for initialization
	void Start () {
        R = this.GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		//Après un temps restore la transparance du matérau
		if (R.material.color.a < max_alpha && fadingTimer <= 0.0f) {
			c = R.material.color;
			c.a += fading_speed;
			R.material.color = c;
		} else if(fadingTimer > 0.0f) {
			fadingTimer -= Time.deltaTime;

		}
		//Puis remet le matériau en mode opaque
		if(c.a >= 1.0f)
			ChangeRenderMode(R.material, BlendMode.Opaque);
	}
	/// <summary>
	/// Code from http://answers.unity3d.com/questions/1004666/change-material-rendering-mode-in-runtime.html
	/// Permet de changer le mode de rendu du shader "Standard" d'Unity
	/// </summary>
	public enum BlendMode
	{
		Opaque,
		Cutout,
		Fade,
		Transparent
	}

	public static void ChangeRenderMode(Material standardShaderMaterial, BlendMode blendMode)
	{
		switch (blendMode)
		{
		case BlendMode.Opaque:
			standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
			standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
			standardShaderMaterial.SetInt("_ZWrite", 1);
			standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
			standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
			standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
			standardShaderMaterial.renderQueue = -1;
			break;
		case BlendMode.Cutout:
			standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
			standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
			standardShaderMaterial.SetInt("_ZWrite", 1);
			standardShaderMaterial.EnableKeyword("_ALPHATEST_ON");
			standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
			standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
			standardShaderMaterial.renderQueue = 2450;
			break;
		case BlendMode.Fade:
			standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			standardShaderMaterial.SetInt("_ZWrite", 0);
			standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
			standardShaderMaterial.EnableKeyword("_ALPHABLEND_ON");
			standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
			standardShaderMaterial.renderQueue = 3000;
			break;
		case BlendMode.Transparent:
			standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
			standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			standardShaderMaterial.SetInt("_ZWrite", 0);
			standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
			standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
			standardShaderMaterial.EnableKeyword("_ALPHAPREMULTIPLY_ON");
			standardShaderMaterial.renderQueue = 3000;
			break;
		}

	}
	// -----
}
