﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

//https://docs.unity3d.com/Manual/nav-CouplingAnimationAndNavigation.html
public class playerController : MonoBehaviour {

    public Camera cam;
	private NavMeshAgent agent;
    private Animator animator;

    protected GameObject target;
    protected Vector3 destination;
	private ActorStats stats;

    public float attackRange = 2.0f;
    public float attackSpeed = 2.0f;

    private float attackDelay = 0.0f;

    //State machine
    public enum State { moving, chasing, attacking,  skill01, iddle, dead}
	public State state = State.iddle;
	State last_state;

	// Use this for initialization
	void Start () {
		agent = this.GetComponent<NavMeshAgent> ();
        animator = this.GetComponent<Animator>();
		stats = this.gameObject.GetComponent<ActorStats> ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        //GameObject go;
        /*if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "Enemy")
                {
                    target = hit.collider.gameObject;
                    state = State.chasing;
                }
                else
                {
                    destination = hit.point;
                    state = State.moving;
                }
            }
        }*/

		this.transform.LookAt(agent.steeringTarget + transform.forward);
       // animator.SetBool("isAttacking", false);
		animator.SetBool("isRunning", false);
		//animator.SetBool ("isHeavyAttacking", false);

		if (!stats.alive)
			state = State.dead;

        switch (state)
        {
            case State.iddle:
                //Iddle timer ?
                break;
			case State.moving:
				if (Vector3.Distance (this.transform.position, destination) > 0.5f) {
					animator.SetBool ("isRunning", true);
					move ();
				}
                else
                {
                    state = State.iddle;
                    agent.ResetPath();
                }
                break;
			//Merge chase and attacking ?
			case State.chasing:
				if (Vector3.Distance (this.transform.position, target.transform.position) > attackRange) {
					animator.SetBool ("isRunning", true);
					attackMove ();
				}
                else
                {
                    state = State.attacking;
                    attack();
                }
                break;
            case State.attacking:
                if(Vector3.Distance(this.transform.position, target.transform.position) < attackRange)
                {
                    attack();
                }
                else
                {
                    state = State.chasing;
					animator.SetBool ("isRunning", true);
                    attackMove();
                }
                break;
			case State.skill01:
				agent.ResetPath ();
				animator.SetBool ("isHeavyAttacking", true);
				state = last_state;
				break;
			case State.dead:
				animator.SetTrigger ("isDead");
				break;
	        default:
	            //Should not use
	            break;
        }
	}

	public void setTarget(RaycastHit hit){
		if (hit.collider.tag == "Enemy")
		{
			target = hit.collider.gameObject;
			state = State.chasing;
		}
		else
		{
			destination = hit.point;
			state = State.moving;
		}
	
	}

    void move()
    {
        agent.SetDestination(destination);
    }

    void attackMove()
    {
        agent.SetDestination(target.transform.position);
    }

    void attack()
    {
        agent.ResetPath();
		this.transform.LookAt (target.transform.position);
        ActorStats AS = target.GetComponent<ActorStats>();
        if (AS.alive && attackDelay <= 0.0f)
        {
            attackDelay = attackSpeed;
            this.gameObject.GetComponent<ActorStats>().normalAttack(AS);
            target.GetComponent<Animator>().SetBool("isDamaged", true);
            animator.SetBool("isAttacking", true);
            animator.SetInteger("SlashAnim", (animator.GetInteger("SlashAnim") + 1) % 2);
            

        }
		if (!AS.alive) {
			state = State.iddle;
		}
		if(attackDelay > 0.0f)
        {
            attackDelay -= Time.deltaTime;
        }
    }

	public void skill_01(Vector3 dir){
		last_state = state;
		this.transform.forward = dir;
		state = State.skill01;
	}
}
/*
 Animator animatorCtrl;
    NavMeshAgent agent;
    // Use this for initialization
    void Start () {
        animatorCtrl = this.gameObject.GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Z))
        {
            animatorCtrl.SetBool("isRunning", true);
        }
        else
        {
            animatorCtrl.SetBool("isRunning", false);
        }

        if (Input.GetKeyDown(KeyCode.T) && !animatorCtrl.GetCurrentAnimatorStateInfo(0).IsName("HeavyAttack"))
        {
            agent.ResetPath();
            animatorCtrl.SetBool("isAttacking", true);

        }
        else
        {
            animatorCtrl.SetBool("isAttacking", false);
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            agent.Stop();
            animatorCtrl.SetBool("isDead", true);
        }
       


        if (Input.GetKey(KeyCode.S) && !animatorCtrl.GetCurrentAnimatorStateInfo(0).IsName("Slash1"))
        {
            animatorCtrl.SetBool("isHeavyAttacking", true);
        }
        else
        {
            animatorCtrl.SetBool("isHeavyAttacking", false);
        }

    }
}

     */
