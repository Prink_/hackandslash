﻿using UnityEngine;
using System.Collections;

public class cameraController : MonoBehaviour {

    public GameObject player;
    public float distance = 2.0f;
    public float rotation = 0.0f;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectsWithTag("Player")[0];
	}

	//Regarde si il y a un objet entre la camera et le joueur. Si c'est le cas elle rend l'objet translucide.
    void check_ocluders()
    {
        RaycastHit[] hits;
        float d2player = Vector3.Distance(this.transform.position, player.transform.position);
        int layer = 1 << 2; //Layer mask -> touche seulement les object dans le layer "IgnoreRaycast"

        hits = Physics.RaycastAll(this.transform.position, this.transform.forward, d2player - 0.5f, layer);

        foreach(RaycastHit hit in hits)
        {
            Renderer R = hit.collider.GetComponent<Renderer>();
            if (R == null)
                continue;
            FadingController FC = R.GetComponent<FadingController>();
            if(FC == null)
            {
                FC = R.gameObject.AddComponent<FadingController>();
            }
			FC.makeTranslucent();
        }

    }

	// Update is called once per frame
	void Update () {
        check_ocluders();
	}

	void LateUpdate(){
		//Gère la position de la caméra en fonction des input joueurs
		distance = Mathf.Clamp(distance + -Input.GetAxis("Mouse ScrollWheel") * 2.0f, 0, 20);
		this.transform.position = new Vector3(player.transform.position.x + Mathf.Cos(rotation) * distance, player.transform.position.y + distance, player.transform.position.z - Mathf.Sin(rotation) * distance);
		this.transform.LookAt(player.transform.position + Vector3.up);
	}
}
