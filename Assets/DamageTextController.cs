﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTextController : MonoBehaviour {

	public GameObject cam;
	public GameObject actor;
	public int dmg;
	public float timer = 10.0f;
	public float cd;
	public Color color;

	// Use this for initialization
	void Start () {
		if(cam == null)
			cam = GameObject.FindGameObjectWithTag ("MainCamera");
		cd = timer;
		//this.transform.up = Vector3.up;
		this.GetComponent<TextMesh> ().text = dmg.ToString();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if(actor != null)
			this.transform.position = actor.transform.position + new Vector3 (0.0f, 2.0f + 2.0f* (1.0f-(cd/timer)) , 0.0f);
		this.transform.LookAt (cam.transform.position);
        this.transform.Rotate(Vector3.up, 180);

		color.a = (cd / timer);
		this.GetComponent<TextMesh>().color = color;
		cd -= Time.deltaTime;

        if (cd < 0)
            Destroy(this.gameObject);
	}
}
