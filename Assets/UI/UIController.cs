﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	public ActorStats player;
	private Image HPBar;
	float HPw;
	float HPh;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<ActorStats>();
		if (player == null) {
			Debug.LogError ("No player found");
		}

		for(int i =0 ; i < this.transform.childCount; i++){
			if (this.transform.GetChild (i).name == "CurrentHP") {
				HPBar = this.transform.GetChild (i).GetComponent<Image> ();
			}
		}
		if (HPBar != null) {
			HPw = HPBar.rectTransform.rect.width;
			HPh = HPBar.rectTransform.rect.height;
		} 
		else 
		{
			Debug.LogError ("No bar found");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (player != null && HPBar != null) {
			this.HPBar.rectTransform.sizeDelta = new Vector2 (((float)player.HP_current / (float)player.HP_max) * HPw, HPh);
		}
	}
}
