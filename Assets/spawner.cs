﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


[Serializable]
public class wave{
	public GameObject monster;
	public int numbers;
	public Vector3 spawnPosition;
	public float spawnTime;
	public Vector3[] path;
	private bool spawned = false;
	private GameObject[] monsters;

	public bool hasSpawned(){
		return spawned;
	}

	public void spawn(spawner spawner){
		spawned = true;
		monsters = new GameObject[numbers];
		for (int i = 0; i < numbers; i++) {
			monsters[i] = spawner.Instantiate<GameObject>(monster, spawnPosition + new Vector3 (i, 0, 0), Quaternion.identity);
			monster c = monsters [i].GetComponent<monster> ();
			if (c != null) {
				c.setPath (path);
			}
		}
	}
}

public class spawner : MonoBehaviour {
	public wave[] waves;
	private float startTime;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < waves.Length; i++) {
			if (!waves [i].hasSpawned() && Time.time - startTime > waves [i].spawnTime) {
				waves [i].spawn (this);
			}
		}
	}
}
