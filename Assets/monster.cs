﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class monster : MonoBehaviour {
	private Transform player;
	public float dist_aggro;
	public float end_aggro_dist;
	public float attack_dist;
	public float attack_time;
	private float destructionTimer = 10.0f;
	bool chase;
    bool attack;
	NavMeshAgent nav;
	Animator animatorCtrl;
	float lastAttack;
	Vector3[] path;
	int moveCount;
	ActorStats AS;
	bool stopped;

	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag ("Player").transform;
		nav = GetComponent<NavMeshAgent>();
		animatorCtrl = gameObject.GetComponent<Animator>();
		chase = false;
		lastAttack = 0;
		if(path == null)
			path = new Vector3[0];
		moveCount = 0;
		AS = this.GetComponent<ActorStats> ();
        attack = false;
		stopped = false;
	}
	
	// Update is called once per frame
	void Update (){
		if (AS.alive) {
            animatorCtrl.SetBool("isRunning", false);
            if (!chase && moveCount < path.Length) {
				if (Vector3.Distance (transform.position, path [moveCount]) <= 1) {
					nav.ResetPath ();
					moveCount++;
				} else {
                    nav.destination = path [moveCount];
					animatorCtrl.SetBool("isRunning", true);
				}
			}
			if (!chase && dist () <= dist_aggro) {
				chase = true;
			}
			if (chase && dist () >= end_aggro_dist) {
				chase = false;
				nav.ResetPath ();
			}
			if (chase) {
                nav.destination = player.position;
                if (!attack) { animatorCtrl.SetBool("isRunning", true); }
                
            }
			animatorCtrl.SetBool ("isAttacking", false);
			if (dist () <= attack_dist) {
				nav.ResetPath ();
				if (Time.time - lastAttack >= attack_time && player.GetComponent<ActorStats>().alive) {
					lastAttack = Time.time;     
                    animatorCtrl.SetBool ("isAttacking", true);
                    attack = true;
					AS.normalAttack (player.GetComponent<ActorStats> ());
                    player.GetComponent<Animator>().SetBool("isDamaged", true);
				}
            }else
            {
                attack = false;
            }
		} else {
			if (!stopped) {
				stopped = true;
				GetComponent<NavMeshAgent> ().Stop ();
			}
			animatorCtrl.SetTrigger ("isDead");
			destructionTimer -= Time.deltaTime;
			if (destructionTimer < 0)
				Destroy (this.gameObject);
		}
	}
	
	float dist(){
		return Vector3.Distance (transform.position, player.position);
	}

	public void setPath(Vector3[] npath){
		path = npath;
		moveCount = 0;
	}
}
