﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActorStats : MonoBehaviour {
	//Represent the stats of any actor in the scene

	//Hard stats
	[Range (0, 1000)] public int HP_max;
	[Range (0, 100)] public int strenght;


	//Soft stats
	[Range (0, 1000)] public int HP_current;
	public bool alive;

    GameObject dmgTxt;

    //Attack scalings
        //Cleave
        float cleaveScale = 1.5f;
        int cleaveFlat = 0;


	// Use this for initialization
	void Start () {
		//HP_max must be above 0
		alive = true;
		if(HP_max <=0){
			HP_max = 1;
		}
		HP_current = Mathf.Min (HP_current, HP_max);
		dmgTxt = Resources.Load ("Prefab/DamageText") as GameObject;

	}
	
	// Update is called once per frame
	void Update () {
	}


	/* *************************************************** */

	public void takeDamage(int damage){
		HP_current -= Mathf.Abs(damage); //Can't take negative damage
        if (dmgTxt != null)
        {
            GameObject floating_txt = Instantiate(dmgTxt);
			floating_txt.GetComponent<DamageTextController>().actor = this.gameObject;
			floating_txt.GetComponent<DamageTextController>().dmg = damage;
        } else
        {
            Debug.LogError("No \"DamageText\" prefab is set to \"ActorStats\" script.");
        }

        if (HP_current <= 0) {
			HP_current = 0;
			death();
		}
	}

	public void gainHealth(int heal){
		HP_current += Mathf.Abs(heal);

		if (HP_current > HP_max) { //Current HP can't exceed max HP
			HP_current = HP_max;
		}
	}

	public void death(){
		alive = false;
		Debug.Log(this.name + " has died !");
	}

    public void normalAttack(ActorStats target)
    {
        target.takeDamage(this.strenght);
    }

    public void cleaveAttack(List<Collider> targets)
    {
        foreach(Collider target in targets)
        {
			if(target.GetComponent<ActorStats>().alive)
            	target.GetComponent<ActorStats>().takeDamage((int)Mathf.Floor(this.cleaveScale * this.strenght) + cleaveFlat);
        }
    }
}
