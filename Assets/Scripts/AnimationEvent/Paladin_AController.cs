﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Paladin_AController : MonoBehaviour {
    AudioSource footstep;
    NavMeshAgent agent;
    Animator animatorCtrl;
    public ParticleSystem stepDust;
	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        stepDust = Instantiate(stepDust);
        stepDust.transform.parent = this.gameObject.transform;
        animatorCtrl = GetComponent<Animator>();
        footstep = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        stepDust.transform.position = new Vector3(agent.transform.position.x, agent.transform.position.y + 0.2f, agent.transform.position.z);
        if (animatorCtrl.GetCurrentAnimatorStateInfo(0).IsName("Run"))
        {
            if (!footstep.isPlaying) { footstep.Play(); }
        }
        else
        {
            footstep.Stop();
        }
    }

    public void SpawnParticle(GameObject i)
    {
        if (!stepDust.IsAlive()) {   
            stepDust.Play();
        }
       
    }

    public void ImpactReset()
    {
        animatorCtrl.SetBool("isDamaged", false);
    }

    public void AttackingReset()
    {
        animatorCtrl.SetBool("isAttacking", false);
    }

    public void HeavyAttackReset()
    {
        animatorCtrl.SetBool("isHeavyAttacking", false);
    }
}
