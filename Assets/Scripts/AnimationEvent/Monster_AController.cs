﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_AController : MonoBehaviour {

    Animator animatorCtrl;

    // Use this for initialization
    void Start () {
        animatorCtrl = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ImpactReset()
    {
        animatorCtrl.SetBool("isDamaged", false);
    }
}
